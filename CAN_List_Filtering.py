from pyb import CAN
import utime as time
from umachine import Pin
from pyb import SD
from uos import VfsFat


Pin("PH0", Pin.OUT).on()


print('#TM4C1294 CAN BUS DATALOGGER\n')
print('#Message Number, Time, ID, DLC, DATA\n')



#Init CAN0 with 500 kbit/s
can1 = CAN(500)


#Init SD card module
vfs = VfsFat(SD)
logfile = vfs.open('TB_testing1.txt','a') 
logfile.write('\n\n\n#TM4C1294 CAN BUS DATALOGGER\n')
logfile.write('#Message Number, Time, ID, DLC, DATA\n')

counter = 1

ids =[0x7F1,0x7F2,0x7F3,0x250,0x251]
can1.setMask(ids)

val = can1.read()
start_time = time.ticks_ms()
    
for x in range(1,10000):
    val = can1.read()
    data_string = "%d,%d,%.2x%.2x%.2x%.2x,%.2x,%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x" %(counter, time.ticks_ms(),val[0],val[1],val[2],int(val[3]),int(val[4]),int(val[5]),int(val[6]),int(val[7]),int(val[8]),int(val[9]),int(val[10]),int(val[11]),int(val[12]))
    counter= counter + 1

    print(data_string)
    logfile.write(data_string+'\n')

duration = time.ticks_ms()-start_time
print("Duration: "+str(duration)+" ms")
logfile.flush()      




